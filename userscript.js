// ==UserScript==
// @name    freelancer
// @author  coderaiders
// @namespace   freelancer
// @include   https://www.freelancer.com/users/messages/*
// @version   1
// @run     onload
// @grant     none
// ==/UserScript==
(function($) {
  var counter = {
      get: function() {
        return localStorage.getItem('lastCount') || 0;
      },

      set: function(newVal) {
        lastCount = newVal;
        localStorage.setItem('lastCount', newVal || 0);
      }
    },
    lastCount = counter.get(),
    chatCount = 0,
    chatNotify;

  function updateCount() {
    chatNotify = null;
  }

  function notify(msg, callback) {
    // There is already a notification
    if (chatNotify) {
      return;
    }

    chatNotify = new window.Notification(msg, {
      body: 'Click on this notification if you don\'t want to see it'
    });

    // Stop showing notifications if user closes a notification
    chatNotify.onclick = function() {
      callback();
      updateCount();
    };
    chatNotify.onclose = updateCount;
  }

  // Request notification permission
  if ( ('Notification' in window)) {
    window.Notification.requestPermission();
  }

  setInterval(function() {
    var $threads = $('.messages-thread li.thread'),
      $chat = $('#chat');

    if ($threads && $threads.length > lastCount) {
      notify('Inbox message on freelancer.com', function() {
        counter.set($threads.length);
      });
    }

    // Show notification if there is a new chat window
    if ($chat && $chat.children().length > chatCount) {
      notify('New chat message on freelancer.com', function() {
        chatCount = $chat.children().length;
      });
    }
    else if ($chat.children().length === 0) {
      chatCount = 0;
    }
  }, 500);
} (window.jQuery));
